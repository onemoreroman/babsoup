import pandas as pd
import numpy as np
from catboost import CatBoostRegressor

general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')

train = pd.read_csv('data/train.csv', encoding='utf-8', usecols=general_cols)
test = pd.read_csv('data/test.csv', encoding='utf-8')

# CAT features
train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday
test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday

# target
train['RPC'] = train.Revenue / train.Clicks

train['month_of_year'] = pd.to_datetime(train['Date']).dt.month
test['month_of_year'] = pd.to_datetime(test['Date']).dt.month


train_extra_new = pd.read_csv('cpu_precalculation/train_extra._csv')
train = train.merge(train_extra_new, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])
test = test.merge(train_extra_new, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])

##################
# Adding period feature
super_train = pd.concat([train[['Date', 'AdGroupId']], test[['Date', 'AdGroupId']]], ignore_index=True)
campaign_id = super_train.groupby(['AdGroupId'], as_index=False).agg({'Date': lambda s: np.unique(s).shape[0]})
campaign_id = campaign_id.rename(columns={'Date': 'AdGroupLen'})
train = pd.merge(left=train, right=campaign_id, on='AdGroupId', how='left')
test = pd.merge(left=test, right=campaign_id, on='AdGroupId', how='left')

# Add till the end feature
#ad_till_end = pd.read_csv('cpu_precalculation/rest_ad.csv.zip')
#train = pd.merge(left=train, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')
#test = pd.merge(left=test, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')

# Previous day views feature
num_ads = pd.read_csv('cpu_precalculation/prev_day_nn._csv')
train = pd.merge(left=train, right=num_ads, on=['Date', 'ClientName'], how='left')
test = pd.merge(left=test, right=num_ads, on=['Date', 'ClientName'], how='left')

extra_data_g = pd.read_csv('extra_data/google_sweden._csv')
extra_data_g['CountryCriteriaId'] = 2752
train = pd.merge(left=train, right=extra_data_g, on=['Date', 'CountryCriteriaId'], how='left')
test = pd.merge(left=test, right=extra_data_g, on=['Date', 'CountryCriteriaId'], how='left')
##################


# X1 = train[train['day_of_year'] < 100].drop(['RPC', 'Revenue', 'Date', 'Clicks'], axis=1)
# y1 = train[train['day_of_year'] < 100]['RPC']
# weights = train[train['day_of_year'] < 100]['Clicks']
#
# X1 = X1[y1 < 75]
# weights = weights[y1 < 75]
# y1 = y1[y1 < 75]
#
# X2 = train[train['day_of_year'] >= 100].drop(['RPC', 'Revenue', 'Date', 'Clicks'], axis=1)
# y2 = train[train['day_of_year'] >= 100]['RPC']
#
# for rs in [10, 20, 30]:
#     model = CatBoostRegressor(iterations=1000, random_state=rs, loss_function='RMSE', task_type='GPU')
#     model.fit(X1, y1, cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], sample_weight=weights.values, eval_set=(X2, y2))
# exit()

X = train.drop(['RPC', 'Revenue', 'Date', 'Clicks'], axis=1)
y = train['RPC']

model = CatBoostRegressor(iterations=600, random_state=1, loss_function='RMSE', task_type='CPU')
model.fit(X, y, cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], sample_weight=train['Clicks'].values)

X_pred = test.drop(['RowId', 'Date'], axis=1)
y_pred = model.predict(X_pred)
y_pred = np.round(y_pred, 5)

sample_subm = test[['RowId', 'ClientName']].copy()
sample_subm['RPC'] = y_pred

sample_subm.to_csv('submission_month_.csv', index=False, encoding='utf-8')





import pandas as pd
import numpy as np
# Best Borsch = -0.38
# Best Minestrone + 0.25
# Bouillabaisse + 0.25


general_cols = 'Date,ClientName,Revenue,Clicks'
general_cols = general_cols.split(',')


list_companies = {'Pho': 1290, 'Minestrone': 1335, 'Borscht': 1600, 'Miso': 565, 'Bouillabaisse': 265, 'TomYum': 188, 'Gazpacho': 196}

for company_name, num_iterations in list_companies.items():

    train = pd.read_csv('../data/train_{}.csv'.format(company_name), encoding='utf-8', usecols=general_cols)
    test = pd.read_csv('../data/test_{}.csv'.format(company_name), encoding='utf-8')

    avg = train.Revenue.sum() / train.Clicks.sum()
    if company_name == 'Pho':
        avg = 27.73
    if company_name == 'Minestrone':
        avg = 7
    if company_name == 'Borscht':
        avg = 6.41
    if company_name == 'Miso':
        avg = 1.0
    if company_name == 'Bouillabaisse':
        avg = 5.74
    if company_name == 'TomYum':
        avg = 6.49
    if company_name == 'Gazpacho':
        avg = 55.77
    test['RPC'] = avg

    # sample_subm = test[['RowId', 'RPC', 'ClientName']].copy()
    sample_subm = test[['RowId', 'RPC']].copy()
    sample_subm.to_csv('predicted_avg_{}.csv'.format(company_name), index=False, encoding='utf-8')
    del sample_subm
    print(company_name, 'predicted')

# combining all together
# final_df = pd.DataFrame(columns=['RowId', 'RPC', 'ClientName'])
final_df = pd.DataFrame(columns=['RowId', 'RPC'])
for company_name, num_iterations in list_companies.items():
    predicted_df = pd.read_csv('predicted_avg_{}.csv'.format(company_name))
    final_df = pd.concat([final_df, predicted_df], ignore_index=True)
# order
final_df = final_df.sort_values(by=['RowId'])
final_df.to_csv('submission_cpu_avg.csv', index=False)

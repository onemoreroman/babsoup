import pandas as pd
import numpy as np


general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')


list_companies = {'Pho': 1290, 'Minestrone': 1335, 'Borscht': 1600, 'Miso': 565, 'Bouillabaisse': 265, 'TomYum': 188, 'Gazpacho': 196}

for company_name, num_iterations in list_companies.items():

    train = pd.read_csv('../data/train_{}.csv'.format(company_name), encoding='utf-8', usecols=general_cols)
    test = pd.read_csv('../data/test_{}.csv'.format(company_name), encoding='utf-8')

    # PREPARE train and test
    train['RPC'] = train.Revenue / train.Clicks
    train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday

    test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday
    test = test.drop(['Date'], axis=1)


    def ff(x):
        xx = x[x.Date > '2017-01-01']
        dof = xx['Dayofweek'].values[0]
        avg_dof = x.Revenue.sum() / xx.Clicks.sum()
        return pd.Series([dof, avg_dof], index=['Dayofweek', 'RPC'])


    avg_dof = train.groupby(['Dayofweek'], as_index=False).apply(ff)
    test = pd.merge(left=test, right=avg_dof, on='Dayofweek', how='left')

    sample_subm = test[['RowId', 'RPC']].copy()
    sample_subm.to_csv('predicted_avg_{}.csv'.format(company_name), index=False, encoding='utf-8')
    del sample_subm
    print(company_name, 'predicted')

# combining all together
final_df = pd.DataFrame(columns=['RowId', 'RPC'])
for company_name, num_iterations in list_companies.items():
    predicted_df = pd.read_csv('predicted_avg_{}.csv'.format(company_name))
    final_df = pd.concat([final_df, predicted_df], ignore_index=True)
# order
final_df = final_df.sort_values(by=['RowId'])
final_df.to_csv('submission_cpu_avg.csv', index=False)

import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
import lightgbm as lgb

print('Minestrone')
# tt = pd.read_csv('../data/test.csv')
train = pd.read_csv('../data/train_Pho.csv')  # {'Pho': 1290, 'Minestrone': 1335, 'Borscht': 1600, 'Miso': 565, 'Bouillabaisse': 265, 'TomYum': 188, 'Gazpacho': 196}
# Pho - 2752 - Sweden
# Minestrone - many, main 2752 - Sweden
# Bouillabaisse - many, main 2578, (2724, 2752) - Norway
# Gazpacho - 2752 - Sweden
# TomYum - 2208 - Denmark
# Miso - many, 2246 - Finland
# Borscht - many, 2752 - Sweden


train['RPC'] = train.Revenue / train.Clicks

train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday

# kw = pd.read_csv('../data/extra_kw_structure.csv')
# kw = kw[['KeywordId', 'ClientName', 'CampaignId', 'AdGroupId', 'KeywordMatchType']]  # , 'KeywordId' 'KeywordMatchType'
# kw = kw.drop_duplicates(subset=['ClientName', 'CampaignId', 'AdGroupId'])
# kw = kw.groupby(['ClientName', 'CampaignId', 'AdGroupId'], as_index=False).agg({'KeywordId': len})
# # print(train.shape)
# train = pd.merge(left=train, right=kw, on=['ClientName', 'CampaignId', 'AdGroupId'], how='left')
# print(train.shape)
# exit()

# extra_data1 = pd.read_csv('../extra_data/buy_denmark._csv')
# extra_data1 = pd.read_csv('../extra_data/weather_denmark._csv')
# extra_data2 = pd.read_csv('../extra_data/buy_finland._csv')
# extra_data3 = pd.read_csv('../extra_data/buy_norway._csv')
# extra_data4 = pd.read_csv('../extra_data/buy_sweden._csv')
# extra_data5 = pd.read_csv('../extra_data/google_sweden._csv')
# extra_data1['CountryCriteriaId'] = 2208
# extra_data2['CountryCriteriaId'] = 2246
# extra_data3['CountryCriteriaId'] = 2578
# extra_data4['CountryCriteriaId'] = 2752
# extra_data5['CountryCriteriaId'] = 2752
# train = pd.merge(left=train, right=extra_data1, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data2, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data3, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data4, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data5, on=['Date', 'CountryCriteriaId'], how='left')
# train = train.fillna(0)

# ad_till_end = pd.read_csv('../cpu_precalculation/rest_ad.csv.zip')
# train = pd.merge(left=train, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')

# train['Device'] = train['Device'].astype('category')
# train['ClientName'] = train['ClientName'].astype('category')
# train['CampaignId'] = train['CampaignId'].astype('category')
# train['AdGroupId'] = train['AdGroupId'].astype('category')
# df_extra = pd.read_csv('../data2/extra_kw_structure.csv')

# ++++++++++++++++++++
# ad uniq num per day
# ad_num = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': lambda s: np.unique(s).shape[0]})
# ad_num = ad_num.rename(columns={'AdGroupId': 'AdDateNumU'})
# train = pd.merge(left=train, right=ad_num, on=['Date', 'ClientName'], how='left')
#
# ad_num2 = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': len})
# ad_num2 = ad_num2.rename(columns={'AdGroupId': 'AdDateNum'})
# train = pd.merge(left=train, right=ad_num2, on=['Date', 'ClientName'], how='left')
#
# # activity per country
# country_activity = train[['Date', 'CountryCriteriaId', 'Device']].groupby(['Date', 'CountryCriteriaId'], as_index=False).agg({'Device': len})
# country_activity = country_activity.fillna(0)
# country_activity = country_activity.rename(columns={'Device': 'CountryAdActivity'})
# train = pd.merge(left=train, right=country_activity, on=['Date', 'CountryCriteriaId'], how='left')
# ++++++++++++++++++++


# Adding period feature
campaign_id = train.groupby(['AdGroupId'], as_index=False).agg({'Date': lambda s: np.unique(s).shape[0]})
campaign_id = campaign_id.rename(columns={'Date': 'AdGroupLen'})
train = pd.merge(left=train, right=campaign_id, on='AdGroupId', how='left')

# train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday

'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId'
',MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'


# Date feature transforming
train['Date'] = pd.to_datetime(train['Date'])
train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)
train['Date'] = train['Date']

# NN Before
num_ads = train.groupby(['Date'], as_index=False).agg({'AdGroupId': len})
num_ads['AdGroupId2'] = num_ads['AdGroupId'].rolling(min_periods=2, window=2).sum()
num_ads['AdGroupId'] = num_ads['AdGroupId2'] - num_ads['AdGroupId']
num_ads = num_ads[['Date', 'AdGroupId']].rename(columns={'AdGroupId': 'ad_n'})
train = pd.merge(left=train, right=num_ads, on=['Date'], how='left')


# local_validation_cutoff = '2017-04-10'
local_validation_cutoff = pd.DatetimeIndex(['2017-04-10']).astype(np.int64)[0]

train_loc = train[train.Date < local_validation_cutoff].copy()
validation_loc = train[train.Date >= local_validation_cutoff].copy()

print(train_loc.shape, validation_loc.shape)



def calculate_wrmse(df, truth_col, pred_col, weight_col):
    row_weights = df[weight_col] / df[weight_col].sum().astype(float)
    square_errors = (df[truth_col] - df[pred_col])**2
    avg_error = (square_errors * row_weights).sum()
    return (avg_error)**0.5

zero_benchmark = 0
validation_loc['RPC_pred'] = zero_benchmark
print('Zero benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


avg_benchmark = train_loc.Revenue.sum() / train_loc.Clicks.sum()
validation_loc['RPC_pred'] = avg_benchmark
print('Average benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


# def ff(x):
#     xx = x[x.Date > '2017-01-01']
#     dof = xx['Dayofweek'].values[0]
#     avg_dof = xx.Revenue.sum() / xx.Clicks.sum()
#     return pd.Series([dof, avg_dof], index=['Dayofweek', 'RPC_pred'])
#
#
# avg_dof = train.groupby(['Dayofweek'], as_index=False).apply(ff)
# validation_loc = validation_loc.drop(['RPC_pred'], axis=1)
# validation_loc = pd.merge(left=validation_loc, right=avg_dof, on='Dayofweek', how='left')
# print('Average DOF score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))

validation_loc = validation_loc.drop(['RPC_pred'], axis=1)

to_drop_cols = ['Date', 'Revenue', 'Clicks', 'RPC', 'ClientName', 'CountryCriteriaId']  # ['Date', 'Revenue', 'Clicks', 'RPC']
X_train, y_train = train_loc.drop(to_drop_cols, axis=1), train_loc['RPC']
X_validation, y_validation = validation_loc.drop(to_drop_cols, axis=1), validation_loc['RPC']


model = CatBoostRegressor(iterations=100, loss_function='RMSE', random_state=23,
                          # simple_ctr='Buckets:TargetBorderCount=4'  # BinarizedTargetMeanValue Borders
                          # simple_ctr='Buckets:TargetBorderType=GreedyLogSum'
                          # simple_ctr='Buckets:CtrBorderCount=5'
                          # simple_ctr='BinarizedTargetMeanValue'
                          )

model.fit(X_train, y_train,
          cat_features=[0, 1, 2, 3, 4, 5, 6, 7],
          eval_set=(X_validation, y_validation),
          sample_weight=train_loc['Clicks'].values,
          logging_level='Silent',
          )

validation_loc['RPC_pred'] = model.predict(X_validation)
print('Cat score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))



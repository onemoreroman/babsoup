import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
from sklearn.model_selection import train_test_split


general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')

train = pd.read_csv('../data/train.csv', encoding='utf-8', usecols=general_cols)


## RowId,Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId
#train = train[['Device','CountryCriteriaId','RegionCriteriaId','MetroCriteriaId','CityCriteriaId','MostSpecificCriteriaId','Clicks','Revenue']]
#test = test[['RowId', 'Device','CountryCriteriaId','RegionCriteriaId','MetroCriteriaId','CityCriteriaId','MostSpecificCriteriaId']]

# Добавим целевую переменную
train['RPC'] = train.Revenue / train.Clicks
train['Date'] = pd.to_datetime(train['Date'])
train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)
train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday


X_train, X_validation, y_train, y_validation = train_test_split(
    train.drop(['RPC', 'Clicks', 'Revenue'], axis=1), train['RPC'], train_size=0.8, random_state=1234)


# model = CatBoostRegressor(iterations=3532, depth=2,
#                           learning_rate=0.08,
#                           random_state=10,
#                           od_type='Iter',
#                           od_wait=333)
# model = CatBoostRegressor(iterations=300, depth=5, learning_rate=0.015, random_state=10)
model = CatBoostRegressor(iterations=1500)
WITH_EVAL = False
if WITH_EVAL:
    model.fit(X_train, y_train,
              cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
              eval_set=(X_validation, y_validation),
              )
else:
    model.fit(train.drop(['RPC', 'Clicks', 'Revenue'], axis=1),
              train['RPC'],
              cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
              sample_weight=train['Clicks'].values,
              )
# exit()


test = pd.read_csv('../data/test.csv', encoding='utf-8')
test['Date'] = pd.to_datetime(test['Date'])
test['Date'] = pd.DatetimeIndex(test['Date']).astype(np.int64)
test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday

y_pred = model.predict(test.drop('RowId', axis=1))
y_pred = np.round(y_pred, 6)

sample_subm = test[['RowId']].copy()
sample_subm['RPC'] = y_pred

sample_subm.to_csv('my_submission_it.csv', index=False, encoding='utf-8')
import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
from sklearn.model_selection import train_test_split


general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')

# train = pd.read_csv('../data/train.csv', encoding='utf-8', usecols=general_cols, nrows=10000)
# test = pd.read_csv('../data/test.csv', encoding='utf-8', nrows=10000)
train = pd.read_csv('../data/train.csv', encoding='utf-8', usecols=general_cols)
test = pd.read_csv('../data/test.csv', encoding='utf-8')

# cat_ff = ['Device', 'CountryCriteriaId', 'RegionCriteriaId', 'MetroCriteriaId', 'CityCriteriaId',
#           'MostSpecificCriteriaId', 'ClientName', 'CampaignId', 'AdGroupId', 'Dayofweek']

## RowId,Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId
#train = train[['Device','CountryCriteriaId','RegionCriteriaId','MetroCriteriaId','CityCriteriaId','MostSpecificCriteriaId','Clicks','Revenue']]
#test = test[['RowId', 'Device','CountryCriteriaId','RegionCriteriaId','MetroCriteriaId','CityCriteriaId','MostSpecificCriteriaId']]

# Добавим целевую переменную
train['RPC'] = train.Revenue / train.Clicks

# Add DayOfWeek feature
# train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday
# test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday

# Adding period feature
super_train = pd.concat([train[['Date', 'AdGroupId']], test[['Date', 'AdGroupId']]], ignore_index=True)
campaign_id = super_train.groupby(['AdGroupId'], as_index=False).agg({'Date': lambda s: np.unique(s).shape[0]})
campaign_id = campaign_id.rename(columns={'Date': 'CampaignLen'})
train = pd.merge(left=train, right=campaign_id, on='AdGroupId', how='left')
test = pd.merge(left=test, right=campaign_id, on='AdGroupId', how='left')

# ad_till_end = pd.read_csv('../cpu_precalculation/rest_ad.csv.zip')
# train = pd.merge(left=train, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')
# test = pd.merge(left=test, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')

# Adding buy word feature
# extra_data1 = pd.read_csv('../extra_data/buy_denmark._csv')
# extra_data2 = pd.read_csv('../extra_data/buy_finland._csv')
# extra_data3 = pd.read_csv('../extra_data/buy_norway._csv')
# extra_data4 = pd.read_csv('../extra_data/buy_sweden._csv')
# extra_data1['CountryCriteriaId'] = 2208
# extra_data2['CountryCriteriaId'] = 2246
# extra_data3['CountryCriteriaId'] = 2578
# extra_data4['CountryCriteriaId'] = 2752
# train = pd.merge(left=train, right=extra_data1, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data2, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data3, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data4, on=['Date', 'CountryCriteriaId'], how='left')
# test = pd.merge(left=test, right=extra_data1, on=['Date', 'CountryCriteriaId'], how='left')
# test = pd.merge(left=test, right=extra_data2, on=['Date', 'CountryCriteriaId'], how='left')
# test = pd.merge(left=test, right=extra_data3, on=['Date', 'CountryCriteriaId'], how='left')
# test = pd.merge(left=test, right=extra_data4, on=['Date', 'CountryCriteriaId'], how='left')
# train = train.fillna(0)


# ++++++++++++++++++++
# ad uniq num per day
# ad_num = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': lambda s: np.unique(s).shape[0]})
# ad_num = ad_num.rename(columns={'AdGroupId': 'AdDateNumU'})
# train = pd.merge(left=train, right=ad_num, on=['Date', 'ClientName'], how='left')
#
# ad_num2 = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': len})
# ad_num2 = ad_num2.rename(columns={'AdGroupId': 'AdDateNum'})
# train = pd.merge(left=train, right=ad_num2, on=['Date', 'ClientName'], how='left')

# activity per country
# country_activity = train[['Date', 'CountryCriteriaId', 'Device']].groupby(['Date', 'CountryCriteriaId'], as_index=False).agg({'Device': len})
# country_activity_t = test[['Date', 'CountryCriteriaId', 'Device']].groupby(['Date', 'CountryCriteriaId'], as_index=False).agg({'Device': len})
# country_activity = country_activity.fillna(0)
# country_activity_t = country_activity_t.fillna(0)
# country_activity = country_activity.rename(columns={'Device': 'CountryAdActivity'})
# country_activity_t = country_activity_t.rename(columns={'Device': 'CountryAdActivity'})
# train = pd.merge(left=train, right=country_activity, on=['Date', 'CountryCriteriaId'], how='left')
# test = pd.merge(left=test, right=country_activity_t, on=['Date', 'CountryCriteriaId'], how='left')
# ++++++++++++++++++++

# Add till the end feature
ad_till_end = pd.read_csv('../cpu_precalculation/rest_ad.csv.zip')
train = pd.merge(left=train, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')
test = pd.merge(left=test, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')

# Previous day views feature
num_ads = pd.read_csv('../cpu_precalculation/prev_day_nn._csv')
train = pd.merge(left=train, right=num_ads, on=['Date', 'ClientName'], how='left')
test = pd.merge(left=test, right=num_ads, on=['Date', 'ClientName'], how='left')

# Date feature transforming
train['Date'] = pd.to_datetime(train['Date'])
train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)

test['Date'] = pd.to_datetime(test['Date'])
test['Date'] = pd.DatetimeIndex(test['Date']).astype(np.int64)


# X_train, X_validation, y_train, y_validation = train_test_split(
#     train.drop(['RPC', 'Clicks', 'Revenue'], axis=1), train['RPC'], train_size=0.8, random_state=32)

local_validation_cutoff = pd.DatetimeIndex(['2017-04-10']).astype(np.int64)[0]
X_train = train[train.Date < local_validation_cutoff].drop(['RPC', 'Clicks', 'Revenue'], axis=1)
y_train = train[train.Date < local_validation_cutoff]['RPC']
X_validation = train[train.Date >= local_validation_cutoff].drop(['RPC', 'Clicks', 'Revenue'], axis=1)
y_validation = train[train.Date >= local_validation_cutoff]['RPC']


# model = CatBoostRegressor(iterations=3532, depth=2,
#                           learning_rate=0.08,
#                           random_state=10,
#                           od_type='Iter',
#                           od_wait=333)
# model = CatBoostRegressor(iterations=300, depth=5, learning_rate=0.015, random_state=10)
model = CatBoostRegressor(iterations=1360, random_state=1)
WITH_EVAL = False
if WITH_EVAL:
    model.fit(X_train, y_train,
              cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9],
              eval_set=(X_validation, y_validation),
              )
    print('best iteration found')

    feature_importances = model.get_feature_importance(X_train, y_train, cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9])
    feature_names = X_train.columns
    for score, name in sorted(zip(feature_importances, feature_names), reverse=True):
        print('{}: {}'.format(name, score))

    exit()
else:
    model.fit(train.drop(['RPC', 'Clicks', 'Revenue'], axis=1),
              train['RPC'],
              cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9],
              #sample_weight=train['Clicks'].values,
              )
# exit()


y_pred = model.predict(test.drop('RowId', axis=1))
y_pred = np.round(y_pred, 6)

sample_subm = test[['RowId']].copy()
sample_subm['RPC'] = y_pred

sample_subm.to_csv('my_submission_ff.csv', index=False, encoding='utf-8')
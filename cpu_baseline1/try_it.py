import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
import lightgbm as lgb

train = pd.read_csv('../data/train_Gazpacho.csv')  # Gazpacho TomYum
train['RPC'] = train.Revenue / train.Clicks

train['Device'] = train['Device'].astype('category')
train['ClientName'] = train['ClientName'].astype('category')
train['CampaignId'] = train['CampaignId'].astype('category')
train['AdGroupId'] = train['AdGroupId'].astype('category')
# df_extra = pd.read_csv('../data2/extra_kw_structure.csv')

# train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday

'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId'
',MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'


local_validation_cutoff = '2017-04-10'

train_loc = train[train.Date < local_validation_cutoff].copy()
validation_loc = train[train.Date >= local_validation_cutoff].copy()

print(train_loc.shape, validation_loc.shape)



def calculate_wrmse(df, truth_col, pred_col, weight_col):
    row_weights = df[weight_col] / df[weight_col].sum().astype(float)
    square_errors = (df[truth_col] - df[pred_col])**2
    avg_error = (square_errors * row_weights).sum()
    return (avg_error)**0.5

zero_benchmark = 0
validation_loc['RPC_pred'] = zero_benchmark
print('Zero benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


avg_benchmark = train_loc.Revenue.sum() / train_loc.Clicks.sum()
validation_loc['RPC_pred'] = avg_benchmark
print('Average benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))

validation_loc = validation_loc.drop(['RPC_pred'], axis=1)

X_train, y_train = train_loc.drop(['Date', 'Revenue', 'Clicks', 'RPC'], axis=1), train_loc['RPC']
X_validation, y_validation = validation_loc.drop(['Date', 'Revenue', 'Clicks', 'RPC'], axis=1), validation_loc['RPC']


model = CatBoostRegressor(iterations=350, loss_function='RMSE', random_state=33,
                          # simple_ctr='Buckets:TargetBorderCount=4'  # BinarizedTargetMeanValue Borders
                          # simple_ctr='Buckets:TargetBorderType=GreedyLogSum'
                          # simple_ctr='Buckets:CtrBorderCount=5'
                          # simple_ctr='BinarizedTargetMeanValue'
                          )

model.fit(X_train, y_train,
          cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8],
          eval_set=(X_validation, y_validation),
          sample_weight=train_loc['Clicks'].values,
          logging_level='Silent',
          )

validation_loc['RPC_pred'] = model.predict(X_validation)
print('Cat score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


if True:
    lgb_train = lgb.Dataset(X_train, y_train)
    lgb_eval = lgb.Dataset(X_validation, y_validation, reference=lgb_train)
    params = {
        #'task': 'train',
        'categorical_feature': [0, 1, 2, 3, 4, 5, 6, 7, 8],
        #'boosting_type': 'gbdt',
        'objective': 'regression',
        #'metric': {'l2', 'auc'},
        #'num_leaves': 31,
        #'learning_rate': 0.05,
        #'feature_fraction': 0.9,
        #'bagging_fraction': 0.8,
        #'bagging_freq': 5,
        'verbose': 0
    }
    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=100,
                    valid_sets=lgb_eval,
                    #early_stopping_rounds=25
                    )


    validation_loc['RPC_pred'] = gbm.predict(X_validation, num_iteration=gbm.best_iteration)
    print('LGB score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))

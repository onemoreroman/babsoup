import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
from sklearn.model_selection import train_test_split
from catboost import cv, Pool
from sklearn.model_selection import StratifiedKFold

general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')


list_companies = {'Pho': 1290, 'Minestrone': 1335, 'Borscht': 1600, 'Miso': 565, 'Bouillabaisse': 265, 'TomYum': 188, 'Gazpacho': 196}

for company_name, num_iterations in list_companies.items():

    train = pd.read_csv('../data/train_{}.csv'.format(company_name), encoding='utf-8', usecols=general_cols)
    test = pd.read_csv('../data/test_{}.csv'.format(company_name), encoding='utf-8')

    # PREPARE train and test
    train['RPC'] = train.Revenue / train.Clicks
    train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday

    test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday
    test = test.drop(['Date'], axis=1)

    # FIT
    X = train.drop(['RPC', 'Clicks', 'Revenue', 'Date'], axis=1)
    y = train['RPC']

    model = CatBoostRegressor(iterations=num_iterations, random_state=10)
    model.fit(X, y, cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    # PREDICTION
    y_pred = model.predict(test.drop('RowId', axis=1))
    y_pred = np.round(y_pred, 6)

    sample_subm = test[['RowId']].copy()
    sample_subm['RPC'] = y_pred
    sample_subm.to_csv('predicted_{}.csv'.format(company_name), index=False, encoding='utf-8')
    del sample_subm
    print(company_name, 'predicted')

# combining all together
final_df = pd.DataFrame(columns=['RowId', 'RPC'])
for company_name, num_iterations in list_companies.items():
    predicted_df = pd.read_csv('predicted_{}.csv'.format(company_name))
    final_df = pd.concat([final_df, predicted_df], ignore_index=True)
# order
final_df = final_df.sort_values(by=['RowId'])
final_df.to_csv('submission_cpu.csv', index=False)

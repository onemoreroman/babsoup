import pandas as pd
import numpy as np


# test = pd.read_csv('../data/test.csv', usecols=['Date', 'AdGroupId'], nrows=100)
# train = pd.read_csv('../data/train.csv', usecols=['Date', 'AdGroupId'], nrows=200)
test = pd.read_csv('../data/test.csv', usecols=['Date', 'AdGroupId', 'ClientName'])
train = pd.read_csv('../data/train.csv', usecols=['Date', 'AdGroupId', 'ClientName'])

super_train = pd.concat([train[['Date', 'AdGroupId', 'ClientName']], test[['Date', 'AdGroupId', 'ClientName']]], ignore_index=True)
super_train = super_train.sort_values(by=['Date'])

list_companies = ['Pho', 'Minestrone', 'Borscht', 'Miso', 'Bouillabaisse', 'TomYum', 'Gazpacho']
super_num_ads = pd.DataFrame(columns=['Date', 'ad_n', 'ClientName'])

for company_name in list_companies:
    print(company_name)
    s_train = super_train[super_train['ClientName'] == company_name]
    num_ads = s_train.groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': len})
    num_ads['AdGroupId2'] = num_ads['AdGroupId'].rolling(min_periods=2, window=2).sum()
    num_ads['AdGroupId'] = num_ads['AdGroupId2'] - num_ads['AdGroupId']
    num_ads = num_ads[['Date', 'AdGroupId', 'ClientName']].rename(columns={'AdGroupId': 'ad_n'})

    super_num_ads = pd.concat([super_num_ads, num_ads], ignore_index=True)
    del num_ads
    # train = pd.merge(left=train, right=num_ads, on=['Date', 'ClientName'], how='left')
    # test = pd.merge(left=test, right=num_ads, on=['Date', 'ClientName'], how='left')

super_num_ads.to_csv('prev_day_nn._csv', index=False)

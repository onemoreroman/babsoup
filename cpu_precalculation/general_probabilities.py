import pandas as pd
import numpy as np
from catboost import CatBoostClassifier


general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')

train = pd.read_csv('../data/train.csv', encoding='utf-8', usecols=general_cols)
train_extra = pd.read_csv('../data/extra_kw_structure.csv', encoding='utf-8')


# Добавим целевую переменную
train['RPC'] = np.sign(train.Revenue)
train['Date'] = pd.to_datetime(train['Date'])
train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)
train['Dayofweek'] = pd.to_datetime(train['Date']).dt.weekday


def features(df):
    match_hist = df['KeywordMatchType'].values
    kw_hist = df['KeywordId'].values

    vars = {
        'CampaignId': df['CampaignId'].values[0],
        'ClientName': df['ClientName'].values[0],
        'AdGroupId': df['AdGroupId'].values[0],
        'Match_num_EXACT': list(match_hist).count('EXACT') / len(match_hist),
        'Match_num_BROAD': list(match_hist).count('BROAD') / len(match_hist),
        'Match_num_PHRASE': list(match_hist).count('PHRASE') / len(match_hist),
        'Match_len': len(match_hist),
        'Kw_var': len(np.unique(kw_hist))
    }
    return pd.Series(np.array(list(vars.values())), index=list(vars.keys()))


train_extra = train_extra.groupby(['CampaignId', 'ClientName', 'AdGroupId'], as_index=False).apply(features)
train_extra.to_csv('train_extra._csv', index=False)
exit()
train = train.merge(train_extra, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])


model = CatBoostClassifier(iterations=1111)

model.fit(train.drop(['RPC', 'Clicks', 'Revenue'], axis=1),
          train['RPC'],
          cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
          )


test = pd.read_csv('../data/test.csv', encoding='utf-8')
test['Date'] = pd.to_datetime(test['Date'])
test['Date'] = pd.DatetimeIndex(test['Date']).astype(np.int64)
test['Dayofweek'] = pd.to_datetime(test['Date']).dt.weekday

test = test.merge(train_extra, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])

y_pred = model.predict_proba(test.drop('RowId', axis=1))
y_pred = y_pred[:, 1]

sample_subm = test[['RowId']].copy()
sample_subm['RPC'] = y_pred

sample_subm.to_csv('probabilities._csv', index=False, encoding='utf-8')
import pandas as pd
import numpy as np


# test = pd.read_csv('../data/test.csv', usecols=['Date', 'AdGroupId'], nrows=100)
# train = pd.read_csv('../data/train.csv', usecols=['Date', 'AdGroupId'], nrows=200)
test = pd.read_csv('../data/test.csv', usecols=['Date', 'AdGroupId'])
train = pd.read_csv('../data/train.csv', usecols=['Date', 'AdGroupId'])

super_train = pd.concat([train, test], ignore_index=True)
# super_train['l'] = super_train['AdGroupId']
# super_train = super_train.groupby(['Date', 'AdGroupId'], as_index=False).agg({'l': len})
super_train = super_train.drop_duplicates()
super_train = super_train.sort_values(by=['Date'])

final_ads = test[test['Date'] == '2017-06-30']['AdGroupId'].values

rest_same = []
curr_day = ''
for row_id, row in super_train.iterrows():
    # print(row['Date'], row['AdGroupId'])
    r = super_train[(super_train['Date'] > row['Date']) & (super_train['AdGroupId'] == row['AdGroupId'])].shape[0]
    rest_same.append(r)
    if curr_day != row['Date']:
        curr_day = row['Date']
        print(curr_day)

# print(rest_same, len(rest_same))

super_train['rest_days'] = np.array(rest_same)
super_train.to_csv('rest_ad.csv', index=False)

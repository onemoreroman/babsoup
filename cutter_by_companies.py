import pandas as pd
import numpy as np


general_cols = 'Date,Device,CountryCriteriaId,RegionCriteriaId,MetroCriteriaId,CityCriteriaId,MostSpecificCriteriaId,ClientName,CampaignId,AdGroupId,Revenue,Clicks'
general_cols = general_cols.split(',')

train = pd.read_csv('data/train.csv', encoding='utf-8', usecols=general_cols)
test = pd.read_csv('data/test.csv', encoding='utf-8')

list_companies = ['Pho', 'Minestrone', 'Borscht', 'Miso', 'Bouillabaisse', 'TomYum', 'Gazpacho']

for company_name in list_companies:
    train_tmp = train[train['ClientName'] == company_name]
    train_tmp.to_csv('data/train_{}.csv'.format(company_name), index=False)

    test_tmp = test[test['ClientName'] == company_name]
    test_tmp.to_csv('data/test_{}.csv'.format(company_name), index=False)


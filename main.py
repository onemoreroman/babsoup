import numpy as np
import pandas as pd
from catboost import CatBoostRegressor


train = pd.read_csv('train.csv', usecols=['Date', 'Device', 'CountryCriteriaId', 'RegionCriteriaId', 'MetroCriteriaId', 'CityCriteriaId', 'MostSpecificCriteriaId', 'ClientName', 'CampaignId', 'AdGroupId', 'Revenue', 'Clicks'])

# target
train['RPC'] = train.Revenue / train.Clicks

# date to number
train['Date'] = pd.to_datetime(train['Date'])
train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)

model = CatBoostRegressor(iterations=1397)

model.fit(
    train.drop(['Clicks', 'Revenue', 'RPC'], axis=1),
    train['RPC'],
    cat_features=[1, 2, 3, 4, 5, 6, 7, 8, 9]
)

del train  # freeing memory

test = pd.read_csv('test.csv')
test['Date'] = pd.to_datetime(test['Date'])
test['Date'] = pd.DatetimeIndex(test['Date']).astype(np.int64)

prediction = model.predict(test.drop('RowId', axis=1))

sub = pd.DataFrame(columns=['RowId', 'RPC'])
sub['RowId'] = test['RowId']
sub['RPC'] = np.round(prediction, 6)
sub.to_csv('gp2.csv', index=False)

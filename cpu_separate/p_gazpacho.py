import pandas as pd
import numpy as np
from catboost import CatBoostRegressor, Pool
import lightgbm as lgb

ClientName = 'Gazpacho'
print(ClientName)
# tt = pd.read_csv('../data/test.csv')
train = pd.read_csv('../data/train_{}.csv'.format(ClientName))  # {'Pho': 1290, 'Minestrone': 1335, 'Borscht': 1600, 'Miso': 565, 'Bouillabaisse': 265, 'TomYum': 188, 'Gazpacho': 196}
train = train.sort_values(by=['Date'])
# Pho - 2752 - Sweden
# Minestrone - many, main 2752 - Sweden
# Bouillabaisse - many, main 2578, (2724, 2752) - Norway
# Gazpacho - 2752 - Sweden
# TomYum - 2208 - Denmark
# Miso - many, 2246 - Finland
# Borscht - many, 2752 - Sweden
test = pd.read_csv('../data/test_{}.csv'.format(ClientName))
test = test.sort_values(by=['Date'])


train['RPC'] = train.Revenue / train.Clicks

train['weekday'] = pd.to_datetime(train['Date']).dt.weekday
test['weekday'] = pd.to_datetime(train['Date']).dt.weekday

# Date ....
# train['Date'] = pd.to_datetime(train['Date']).dt.month
# train['Date'] = pd.DatetimeIndex(train['Date']).astype(np.int64)
# train['Date'] = train['Date']


train_extra_new = pd.read_csv('../cpu_precalculation/train_extra._csv')
train = train.merge(train_extra_new, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])
test = test.merge(train_extra_new, how='left', on=['CampaignId', 'ClientName', 'AdGroupId'])

# extra_data1 = pd.read_csv('../extra_data/buy_denmark._csv')
# extra_data1 = pd.read_csv('../extra_data/weather_denmark._csv')
# extra_data2 = pd.read_csv('../extra_data/buy_finland._csv')
# extra_data3 = pd.read_csv('../extra_data/buy_norway._csv')
extra_data4 = pd.read_csv('../extra_data/buy_sweden._csv')
extra_data5 = pd.read_csv('../extra_data/google_sweden._csv')
# extra_data1['CountryCriteriaId'] = 2208
# extra_data2['CountryCriteriaId'] = 2246
# extra_data3['CountryCriteriaId'] = 2578
extra_data4['CountryCriteriaId'] = 2752
extra_data5['CountryCriteriaId'] = 2752
# train = pd.merge(left=train, right=extra_data1, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data2, on=['Date', 'CountryCriteriaId'], how='left')
# train = pd.merge(left=train, right=extra_data3, on=['Date', 'CountryCriteriaId'], how='left')
train = pd.merge(left=train, right=extra_data4, on=['Date', 'CountryCriteriaId'], how='left')
train = pd.merge(left=train, right=extra_data5, on=['Date', 'CountryCriteriaId'], how='left')

test = pd.merge(left=test, right=extra_data4, on=['Date', 'CountryCriteriaId'], how='left')
test = pd.merge(left=test, right=extra_data5, on=['Date', 'CountryCriteriaId'], how='left')


# till end feature
ad_till_end = pd.read_csv('../cpu_precalculation/rest_ad.csv.zip')
train = pd.merge(left=train, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')
test = pd.merge(left=test, right=ad_till_end, on=['Date', 'AdGroupId'], how='left')


# ++++++++++++++++++++
# ad uniq num per day
ad_num = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': lambda s: np.unique(s).shape[0]})
ad_num = ad_num.rename(columns={'AdGroupId': 'AdDateNumU'})
train = pd.merge(left=train, right=ad_num, on=['Date', 'ClientName'], how='left')

ad_num = test[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': lambda s: np.unique(s).shape[0]})
ad_num = ad_num.rename(columns={'AdGroupId': 'AdDateNumU'})
test = pd.merge(left=test, right=ad_num, on=['Date', 'ClientName'], how='left')

ad_num2 = train[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': len})
ad_num2 = ad_num2.rename(columns={'AdGroupId': 'AdDateNum'})
train = pd.merge(left=train, right=ad_num2, on=['Date', 'ClientName'], how='left')

ad_num2 = test[['AdGroupId', 'Date', 'ClientName']].groupby(['Date', 'ClientName'], as_index=False).agg({'AdGroupId': len})
ad_num2 = ad_num2.rename(columns={'AdGroupId': 'AdDateNum'})
test = pd.merge(left=test, right=ad_num2, on=['Date', 'ClientName'], how='left')

# ++++++++++++++++++++


# Adding period feature
super_train = pd.concat([train[['Date', 'AdGroupId']], test[['Date', 'AdGroupId']]], ignore_index=True)
campaign_id = super_train.groupby(['AdGroupId'], as_index=False).agg({'Date': lambda s: np.unique(s).shape[0]})
campaign_id = campaign_id.rename(columns={'Date': 'AdGroupLen'})
train = pd.merge(left=train, right=campaign_id, on='AdGroupId', how='left')
test = pd.merge(left=test, right=campaign_id, on='AdGroupId', how='left')



# NN Before
super_train = pd.concat([train[['Date', 'AdGroupId']], test[['Date', 'AdGroupId']]], ignore_index=True)
num_ads = super_train.groupby(['Date'], as_index=False).agg({'AdGroupId': len})
num_ads['AdGroupId2'] = num_ads['AdGroupId'].rolling(min_periods=2, window=2).sum()
num_ads['AdGroupId'] = num_ads['AdGroupId2'] - num_ads['AdGroupId']
num_ads = num_ads[['Date', 'AdGroupId']].rename(columns={'AdGroupId': 'ad_n'})
train = pd.merge(left=train, right=num_ads, on=['Date'], how='left')

test = pd.merge(left=test, right=num_ads, on=['Date'], how='left')

# NN By Ad
# num_ads = train.groupby(['Date', 'AdGroupId'], as_index=False).agg({'Device': len})
# num_ads['Device2'] = num_ads['Device'].rolling(min_periods=2, window=2).sum()
# num_ads['Device'] = num_ads['Device2'] - num_ads['Device']
# num_ads = num_ads[['Date', 'AdGroupId', 'Device']].rename(columns={'Device': 'ad_id_n'})

super_train = pd.concat([train[['Date', 'AdGroupId', 'Device']], test[['Date', 'AdGroupId', 'Device']]], ignore_index=True)
ad_id_n = super_train.groupby(['Date', 'AdGroupId'], as_index=False).agg({'Device': len})
gg = ad_id_n.groupby(['AdGroupId'])['Device'].apply(lambda x:x.rolling(window=2).sum())
ad_id_n['Device'] = gg.values - ad_id_n['Device'].values
ad_id_n = ad_id_n[['Date', 'AdGroupId', 'Device']].rename(columns={'Device': 'ad_id_n'})
train = pd.merge(left=train, right=ad_id_n, on=['Date', 'AdGroupId'], how='left')
test = pd.merge(left=test, right=ad_id_n, on=['Date', 'AdGroupId'], how='left')

is_tests = False
if is_tests:
    local_validation_cutoff = '2017-04-10'
    # local_validation_cutoff = pd.DatetimeIndex(['2017-04-10']).astype(np.int64)[0]

    train_loc = train[train.Date < local_validation_cutoff].copy()
    validation_loc = train[train.Date >= local_validation_cutoff].copy()

    print(train_loc.shape, validation_loc.shape)



    def calculate_wrmse(df, truth_col, pred_col, weight_col):
        row_weights = df[weight_col] / df[weight_col].sum().astype(float)
        square_errors = (df[truth_col] - df[pred_col])**2
        avg_error = (square_errors * row_weights).sum()
        return (avg_error)**0.5

    zero_benchmark = 0
    validation_loc['RPC_pred'] = zero_benchmark
    print('Zero benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


    avg_benchmark = train_loc.Revenue.sum() / train_loc.Clicks.sum()
    validation_loc['RPC_pred'] = avg_benchmark
    print('Average benchmark score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


    validation_loc = validation_loc.drop(['RPC_pred'], axis=1)

    to_drop_cols = ['Date', 'Revenue', 'Clicks', 'RPC', 'ClientName']
    X_train, y_train = train_loc.drop(to_drop_cols, axis=1), train_loc['RPC']
    X_validation, y_validation = validation_loc.drop(to_drop_cols, axis=1), validation_loc['RPC']


    model = CatBoostRegressor(iterations=500, loss_function='RMSE', random_state=321,
                              # simple_ctr='Buckets:TargetBorderCount=4'  # BinarizedTargetMeanValue Borders
                              # simple_ctr='Buckets:TargetBorderType=GreedyLogSum'
                              # simple_ctr='Buckets:CtrBorderCount=5'
                              # simple_ctr='BinarizedTargetMeanValue'
                              )

    model.fit(X_train, y_train,
              cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8],
              eval_set=(X_validation, y_validation),
              sample_weight=train_loc['Clicks'].values,
              # logging_level='Silent',
              )

    validation_loc['RPC_pred'] = model.predict(X_validation)
    print('Cat score: ', calculate_wrmse(validation_loc, 'RPC', 'RPC_pred', 'Clicks'))


    feature_importances = model.get_feature_importance(Pool(X_train, y_train, cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8]))
    feature_names = X_train.columns
    for score, name in sorted(zip(feature_importances, feature_names), reverse=True):
        print('{}: {}'.format(name, score))

else:

    to_drop_cols = ['Date', 'Revenue', 'Clicks', 'RPC', 'ClientName']
    X_train, y_train = train.drop(to_drop_cols, axis=1), train['RPC']

    model = CatBoostRegressor(iterations=200, loss_function='RMSE', random_state=1,
                              # simple_ctr='Buckets:TargetBorderCount=4'  # BinarizedTargetMeanValue Borders
                              # simple_ctr='Buckets:TargetBorderType=GreedyLogSum'
                              # simple_ctr='Buckets:CtrBorderCount=5'
                              # simple_ctr='BinarizedTargetMeanValue'
                              )
    model.fit(X_train, y_train,
              cat_features=[0, 1, 2, 3, 4, 5, 6, 7, 8],
              # eval_set=(X_validation, y_validation),
              sample_weight=train['Clicks'].values,
              # logging_level='Silent',
              )

    y_pred = model.predict(test.drop(['RowId', 'ClientName', 'Date'], axis=1))
    y_pred = np.round(y_pred, 5)

    sample_subm = test[['RowId']].copy()
    sample_subm['RPC'] = y_pred

    sample_subm.to_csv('sub_1_{}.csv'.format(ClientName), index=False)

    print('mean', y_pred.mean())
